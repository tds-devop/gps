<?php

error_reporting(E_ALL);
ini_set("display_errors", "On");

require_once('./function/SendDataSmartWay.php');

require __DIR__ . '/vendor/autoload.php';

use Google\Cloud\BigQuery\BigQueryClient;

$code = 0;
$jsonData = file_get_contents("php://input");

if ($jsonData !== false AND ! empty($jsonData)) {

    $code = 1;
    $jsonData = json_decode($jsonData);

    //**//send to bigQuery
    $s = new SendData();
    $s->sendToBigQuery($jsonData);

    //end bigQuery

    echo json_encode(array("code" => $code, "message" => "ok"));
} else {
    echo json_encode(array("code" => $code, "message" => "Fail! Wrong parameter."));
}

