<?php

require __DIR__ . '/../vendor/autoload.php';

use Google\Cloud\BigQuery\BigQueryClient;

class SendData {

    public function sendToBigQuery($jsonData) {//decode json data
        $keyPath = './key/gcp-tds-dev-be49a3a1e2fc.json';
        $bigQuery = new BigQueryClient(array(
            'keyFile' => json_decode(file_get_contents($keyPath), true),
        ));


        $vender_id = $jsonData->vender_id;
        $locations_count = $jsonData->locations_count;
        $txt_value = '';
        $num_row = count($jsonData->locations);
        $arr_location = array();
        foreach ($jsonData->locations as $i => $eachLocation) {
            $txt_value .= "(";
            $arr_location[$i][] = isset($eachLocation->max_speed) ? $txt_value .= "'" . $eachLocation->max_speed . "'" . "," : $txt_value .= "'null'" . ",";
            $arr_location[$i][] = isset($eachLocation->mileage) ? $txt_value .= $eachLocation->mileage . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->ext_power_status) ? $txt_value .= $eachLocation->ext_power_status . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->high_acc_count) ? $txt_value .= "'" . $eachLocation->high_acc_count . "'" . "," : $txt_value .= "'null'" . ",";
            $arr_location[$i][] = isset($eachLocation->driver_id) ? $txt_value .= "'" . $eachLocation->driver_id . "'" . "," : $txt_value .= "'null'" . ",";
            $arr_location[$i][] = isset($eachLocation->gsm_cell) ? $txt_value .= $eachLocation->gsm_cell . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->high_de_acc_count) ? $txt_value .= "'" . $eachLocation->high_de_acc_count . "'" . "," : $txt_value .= "'null'" . ",";
            $arr_location[$i][] = isset($eachLocation->hdop) ? $txt_value .= $eachLocation->hdop . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->over_speed_count) ? $txt_value .= "'" . $eachLocation->over_speed_count . "'" . "," : $txt_value .= "'null'" . ",";
            $arr_location[$i][] = isset($eachLocation->recv_utc_ts) ? $txt_value .= "'" . $eachLocation->recv_utc_ts . "'" . "," : $txt_value .= "'null'" . ",";
            $arr_location[$i][] = isset($eachLocation->ext_power) ? $txt_value .= $eachLocation->ext_power . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->engine_status) ? $txt_value .= $eachLocation->engine_status . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->gsm_rssi) ? $txt_value .= $eachLocation->gsm_rssi . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->alt) ? $txt_value .= $eachLocation->alt . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->course) ? $txt_value .= $eachLocation->course . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->speed) ? $txt_value .= $eachLocation->speed . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->gsm_loc) ? $txt_value .= $eachLocation->gsm_loc . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->lon) ? $txt_value .= $eachLocation->lon . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->fix) ? $txt_value .= $eachLocation->fix . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->seq) ? $txt_value .= $eachLocation->seq . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->utc_ts) ? $txt_value .= "'" . $eachLocation->utc_ts . "'" . "," : $txt_value .= "'null'" . ",";
            $arr_location[$i][] = isset($eachLocation->unit_id) ? $txt_value .= "'" . $eachLocation->unit_id . "'" . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->lat) ? $txt_value .= $eachLocation->lat . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->num_sats) ? $txt_value .= $eachLocation->num_sats . "," : $txt_value .= "0" . ",";
            $arr_location[$i][] = isset($eachLocation->license) ? $txt_value .= "'" . $eachLocation->license . "'" . ")" : $txt_value .= "0" . ")";

            if ($i != ($num_row - 1)) {//last row
                $txt_value .= ",";
            }
        }
//change table
        $query = <<<ENDSQL
INSERT INTO `gcp-tds-dev.gps.fleet_data` (vender_id,locations_count,locations) 
VALUES ($vender_id,$locations_count, ARRAY<STRUCT<max_speed STRING, 
mileage INT64,
ext_power_status INT64, 
high_acc_count STRING , 
driver_id STRING, 
gsm_cell INT64,
high_de_acc_count STRING, 
hdop INT64, 
over_speed_count STRING, 
recv_utc_ts TIMESTAMP, 
ext_power INT64, 
engine_status INT64, 
gsm_rssi INT64, 
alt INT64, 
course INT64, 
speed INT64, 
gsm_loc INT64, 
lon FLOAT64, 
fix INT64, 
seq INT64, 
utc_ts TIMESTAMP, 
unit_id STRING, 
lat FLOAT64, 
num_sats INT64, 
license STRING>>
[
$txt_value
]
)
ENDSQL;

        $queryJobConfig = $bigQuery->query($query);
        $queryResults = $bigQuery->runQuery($queryJobConfig);

        if (!$queryResults->isComplete()) {
//throw new Exception('The query failed to complete');
            $query = <<<ENDSQL
INSERT INTO `china-mobile-komect.smart_transsport_freewill.log_error` 
VALUES ('0','The query failed to complete', CURRENT_TIMESTAMP())
ENDSQL;

            $queryJobConfig = $bigQuery->query($query);
            $queryResults = $bigQuery->runQuery($queryJobConfig);
        }
    }

}
